package com.itranswarp.learnjava.practice.plugin.views;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IViewSite;

import com.itranswarp.javapractice.plugin.data.LoadingNode;
import com.itranswarp.javapractice.plugin.data.Node;
import com.itranswarp.javapractice.plugin.data.RootNode;

public class PracticeContentProvider implements ITreeContentProvider {

	static final Object[] EMPTY = new Object[] { new LoadingNode() };

	RootNode root = new RootNode(new LoadingNode());
	IViewSite viewSite;

	public PracticeContentProvider(IViewSite viewSite) {
		this.viewSite = viewSite;
	}

	public void setRootChild(Node node) {
		this.root.setChild(node);
	}

	@Override
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		System.out.println("inputChanged");
	}

	@Override
	public void dispose() {
	}

	@Override
	public Object[] getElements(Object parent) {
		System.out.println("getElements:" + parent);
		if (parent instanceof IViewSite) {
			return root.getChildren().toArray();
		}
		if (parent instanceof Node) {
			return ((Node) parent).getChildren().toArray();
		}
		return EMPTY;
	}

	@Override
	public Object getParent(Object child) {
		System.out.println("getParent: " + child);
		if (child instanceof Node) {
			return ((Node) child).getParent();
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parent) {
		System.out.println("getChildren:" + parent);
		if (parent instanceof Node) {
			return ((Node) parent).getChildren().toArray();
		}
		return EMPTY;
	}

	@Override
	public boolean hasChildren(Object parent) {
		System.out.println("hasChildren:" + parent);
		if (parent instanceof Node) {
			return ((Node) parent).hasChildren();
		}
		return false;
	}
}

package com.itranswarp.learnjava.practice.plugin.views;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;

import com.itranswarp.javapractice.plugin.data.GitNode;
import com.itranswarp.javapractice.plugin.data.LoadingNode;
import com.itranswarp.javapractice.plugin.data.Node;

public class PracticeLabelProvider extends LabelProvider {

	final ISharedImages sharedImages;

	public PracticeLabelProvider(ISharedImages sharedImages) {
		this.sharedImages = sharedImages;
	}

	@Override
	public String getText(Object obj) {
		if (obj instanceof Node) {
			return ((Node) obj).getDisplayName();
		}
		return obj.toString();
	}

	@Override
	public Image getImage(Object obj) {
		if (obj instanceof LoadingNode) {
			LoadingNode loading = (LoadingNode) obj;
			if (loading.isNetworkError()) {
				return this.sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_WARN_TSK).createImage();
			}
			return this.sharedImages.getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED).createImage();
		}
		if (obj instanceof GitNode) {
			GitNode git = (GitNode) obj;
			if (git.isPractice()) {
				return this.sharedImages.getImageDescriptor(ISharedImages.IMG_OBJ_FILE).createImage();
			}
			return this.sharedImages.getImageDescriptor(ISharedImages.IMG_OBJ_FOLDER).createImage();
		}
		return this.sharedImages.getImageDescriptor(ISharedImages.IMG_OBJ_FOLDER).createImage();
	}

}

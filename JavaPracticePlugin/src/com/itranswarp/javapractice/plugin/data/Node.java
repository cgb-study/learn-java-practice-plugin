package com.itranswarp.javapractice.plugin.data;

import java.util.List;

public abstract class Node {

	private Node parent;

	public abstract String getDisplayName();

	public Node getParent() {
		return this.parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public abstract List<Node> getChildren();

	public boolean hasChildren() {
		return !this.getChildren().isEmpty();
	}
}

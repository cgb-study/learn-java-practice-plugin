package com.itranswarp.javapractice.plugin.data;

import java.util.Collections;
import java.util.List;

public class LoadingNode extends Node {

	private boolean networkError;

	public LoadingNode() {
	}

	public LoadingNode(boolean networkError) {
		this.networkError = networkError;
	}

	public boolean isNetworkError() {
		return networkError;
	}

	public void setNetworkError(boolean networkError) {
		this.networkError = networkError;
	}

	@Override
	public List<Node> getChildren() {
		return Collections.emptyList();
	}

	@Override
	public String getDisplayName() {
		return this.networkError ? "Network error" : "Loading...";
	}

	@Override
	public String toString() {
		return String.format("{LoadingNode: networkError=%s}", this.networkError);
	}
}

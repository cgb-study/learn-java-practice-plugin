package com.itranswarp.javapractice.plugin.data;

import java.util.Arrays;
import java.util.List;

public class RootNode extends Node {

	Node child;

	public RootNode(Node child) {
		setChild(child);
	}

	public void setChild(Node child) {
		this.child = child;
		this.child.setParent(this);
	}

	@Override
	public String getDisplayName() {
		return "ROOT";
	}

	@Override
	public List<Node> getChildren() {
		return Arrays.asList(this.child);
	}

}

package com.itranswarp.javapractice.plugin.data;

import java.util.List;

public class GitTree {

	private String sha;
	private String url;
	private List<GitNode> tree;
	private boolean truncated;

	public String getSha() {
		return sha;
	}

	public void setSha(String sha) {
		this.sha = sha;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<GitNode> getTree() {
		return tree;
	}

	public void setTree(List<GitNode> tree) {
		this.tree = tree;
	}

	public boolean isTruncated() {
		return truncated;
	}

	public void setTruncated(boolean truncated) {
		this.truncated = truncated;
	}
}

/**
 * This package provides utility classes for finding type information for generic types.
 *  
 * @author Inderjeet Singh, Joel Leitch
 */
package com.itranswarp.javapractice.plugin.repackaged.com.google.gson.reflect;
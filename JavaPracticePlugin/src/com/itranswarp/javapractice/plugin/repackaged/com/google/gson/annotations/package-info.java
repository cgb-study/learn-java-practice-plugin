/**
 * This package provides annotations that can be used with {@link com.itranswarp.javapractice.repackaged.com.google.gson.Gson}.
 * 
 * @author Inderjeet Singh, Joel Leitch
 */
package com.itranswarp.javapractice.plugin.repackaged.com.google.gson.annotations;
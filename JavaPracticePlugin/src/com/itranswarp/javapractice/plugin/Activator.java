package com.itranswarp.javapractice.plugin;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.prefs.BackingStoreException;

public class Activator implements BundleActivator {

	public static final String PLUGIN_ID = Activator.class.getPackageName();

	private static BundleContext context;
	private static Activator plugin;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		plugin = this;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		plugin = null;
	}

	public static Activator getDefault() {
		return plugin;
	}

	public String getJson() {
		return getValue("json");
	}

	public void setJson(String json) {
		setValue("json", json);
	}

	public String getLastSha() {
		return getValue("lastSha");
	}

	public void setLastSha(String lastSha) {
		setValue("lastSha", lastSha);
	}

	String getValue(String key) {
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(PLUGIN_ID);
		return prefs.get(key, null);
	}

	void setValue(String key, String value) {
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(PLUGIN_ID);
		prefs.put(key, value);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	public ILog getLog() {
		return Platform.getLog(context.getBundle());
	}

}
